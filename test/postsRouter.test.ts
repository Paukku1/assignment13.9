import { jest } from '@jest/globals'
import request from 'supertest'
import { pool } from '../src/db'
import {server} from '../src/index'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe('Testing GET /posts', () => {
    const mockResponse = {
        rows: [
            { id: 1, user_id: 1, comment_id: 1, title: "Hello", content: "World" },
            { id: 2, user_id: 2, comment_id: 2, title: "Hello", content: "World" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test get all', async () => {
        const response = await request(server).get('/posts')
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

})

describe('Testing GET /posts/:id', () => {
    const mockResponse = {
        rows: [
            { id: 1, user_id: 1, comment_id: 1, title: "Hello", content: "World" },
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test get id', async () => {
        const response = await request(server).get('/posts/1')
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

})

describe('Testing DELETE /posts/:id', () => {
    const mockResponse = {
        rows: [
            { id: 1, user_id: 1, comment_id: 1, title: "Hello", content: "World" },
            { id: 2, user_id: 2, comment_id: 2, title: "Hello", content: "World" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test delete id', async () => {
        const response = await request(server).delete('/posts/1')
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual('Delete post by id: 1')
    })

    it('test delete id did not found', async () => {
        const response = await request(server).delete('/posts/5')
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual({"error": "Id did not found"})
    })

})

describe('Testing POST /posts', () => {
    const mockResponse = {
        rows: [
            { id: 1, user_id: 1, comment_id: 1, title: "Hello", content: "World" },
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test post new post without params', async () => {
        const response = await request(server).post('/posts')
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual({"error": "Missing value: Values that needed: user, title and content. Or wrong user"})
    })

    it('test post new post with params, OK.', async () => {
        const response = await request(server).post('/posts')
        .send({ user: 1, title: 'Helloq', content: "Test" })
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual('Added new post')
    })

})

describe('Testing PUT /posts', () => {
    const mockResponse = {
        rows: [
            { id: 1, user_id: 1, comment_id: 1, title: "Hello", content: "World" },
            { id: 2, user_id: 2, comment_id: 2, title: "Hello", content: "World" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test put new post without body', async () => {
        const response = await request(server).put('/posts/7')
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual({"error": "Post id did not found. Try another"})
    })

    it('test put new post with params, OK.', async () => {
        const response = await request(server).put('/posts/1')
        .send({id: 1, user_id: 1, comment_id: 1, title: "Hello5", content: "World" })
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual('value(s) changed')
    })

})