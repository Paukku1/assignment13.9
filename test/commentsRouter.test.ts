import { jest } from '@jest/globals'
import request from 'supertest'
import { pool } from '../src/db'
import {server} from '../src/index'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe('Testing GET /comments/:userid', () => {
    const mockResponse = {
        rows: [
            { id: 1, user_id: 1, post_id: 1, content: "World" },
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test get comment by userid', async () => {
        const response = await request(server).get('/comments/1')
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

    it('test get comment by userid, fail', async () => {
        const response = await request(server).get('/comments/15')
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual({"error": "This user have not any comments"})
    })

})

describe('Testing DELETE /comments/:id', () => {
    const mockResponse = {
        rows: [
            { id: 1, user_id: 1, post_id: 1, content: "World" },
            { id: 2, user_id: 2, post_id: 2, content: "World" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test delete id', async () => {
        const response = await request(server).delete('/comments/1')
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual('Delete comment by id: 1')
    })

    it('test delete id did not found', async () => {
        const response = await request(server).delete('/comments/5')
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual({"error": "Comment for that id did not found"})
    })

})

describe('Testing POST /comments', () => {
    const mockResponse = {
        rows: [
            { id: 1, user_id: 1, post_id: 1, content: "World" },
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test post new comment without params', async () => {
        const response = await request(server).post('/comments')
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual({"error": "Params missing or wrong. Params: user, post_id, content"})
    })

    it('test post new comment with body, OK.', async () => {
        const response = await request(server).post('/comments')
        .send({ user: 1, post_id: 1, content: "Test" })
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual('Added new comment')
    })

    it('test post new comment with body, wrong user', async () => {
        const response = await request(server).post('/comments')
        .send({ user: 31, post_id: 1, content: "Test" })
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual({"error": "Params missing or wrong. Params: user, post_id, content"})
    })

    it('test post new comment with body, wrong post_id', async () => {
        const response = await request(server).post('/comments')
        .send({ user: 1, post_id: 2, content: "Test" })
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual({"error": "Params missing or wrong. Params: user, post_id, content"})
    })

})

describe('Testing PUT /comments', () => {
    const mockResponse = {
        rows: [
            { id: 1, user_id: 1, post_id: 1, content: "World" },
            { id: 2, user_id: 2, post_id: 2, content: "World" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test put comment without body', async () => {
        const response = await request(server).put('/comments/7')
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual({"error": "Comments id did not found. Try another"})
    })

    it('test put comment without body', async () => {
        const response = await request(server).put('/comments/1')
        .send({id: 1})
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual({"error": "Need value what to change. userid, postid or content"})
    })

    it('test put comments with params, OK.', async () => {
        const response = await request(server).put('/comments/1')
        .send({user_id: 1, post_id: 1, content: "Underworld" })
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual('change comment value')
    })

})
