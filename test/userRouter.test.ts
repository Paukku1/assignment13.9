import { jest } from '@jest/globals'
import request from 'supertest'
import { pool } from '../src/db'
import {server} from '../src/index'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe('Testing GET /users', () => {
    const mockResponse = {
        rows: [
            { id: 1, username: 'TestUser', fullname: "Test User", email: "testu@gmail.com" },
            { id: 2, username: 'TestUser2', fullname: "User Test", email: "testu2@gmail.com" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test get all', async () => {
        const response = await request(server).get('/users')
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

})

describe('Testing GET /users/:id', () => {
    const mockResponse = {
        rows: [
            { id: 1, username: 'TestUser', fullname: "Test User", email: "testu@gmail.com" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test get id', async () => {
        const response = await request(server).get('/users/1')
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows[0])
    })

})

describe('Testing DELETE /products/:id', () => {
    const mockResponse = {
        rows: [
            { id: 1, username: 'TestUser', fullname: "Test User", email: "testu@gmail.com" },
            { id: 2, username: 'TestUser', fullname: "Test User", email: "testu@gmail.com" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test delete id', async () => {
        const response = await request(server).delete('/users/1')
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual('Delete user 1')
    })

})

describe('Testing POST /users', () => {
    const mockResponse = {
        rows: [
            { id: 1, username: 'TestUser', fullname: "Test User", email: "testu@gmail.com" },
            { id: 2, username: 'TestUser', fullname: "Test User", email: "testu@gmail.com" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test post new user without params', async () => {
        const response = await request(server).post('/users')
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual('Missing params. Added username, fullname and email?')
    })

    it('test post new user with params, username already exist', async () => {
        const response = await request(server).post('/users')
        .send({id: 3, username: 'TestUser', fullname: "Test User 3", email: "testu3@gmail.com"})
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual({"error": "Username is already exist"})
    })

    it('test post new user with params, OK.', async () => {
        const response = await request(server).post('/users')
        .send({id: 3, username: 'TestUser3', fullname: "Test User 3", email: "testu3@gmail.com"})
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual('Added new user')
    })

})

describe('Testing PUT /users', () => {
    const mockResponse = {
        rows: [
            { id: 1, username: 'TestUser', fullname: "Test User", email: "testu@gmail.com" },
            { id: 2, username: 'TestUser', fullname: "Test User", email: "testu@gmail.com" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    it('test put new user without params', async () => {
        const response = await request(server).put('/users/1')
        expect(response.status).toBe(401)
        expect(response.body).toStrictEqual({"error": "change username, fullname or email"})
    })

    it('test put new user with params, OK.', async () => {
        const response = await request(server).put('/users/1')
        .send({username: 'TestUserX', fullname: "Test User", email: "testu@gmail.com"})
        expect(response.status).toBe(200)
        expect(response.body).toStrictEqual('Change value(s)')
    })

})